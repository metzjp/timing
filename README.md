# timing

Simple extension of timeit module

# Installation 

```shell
python -m pip install git+https://codeberg.org/metzjp/timing.git
```

## Sample usage 

```python 
from timing import timed

... 

@timed(verbose=True)
def function_to_be_timed(...):
    ....
```

