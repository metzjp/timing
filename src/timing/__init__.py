"""
Guts of script form of timeit module
"""
from timing.timing import timeit, TimerContext, timed_simple, timed
__all__ = ["timeit", "TimerContext", "timed", "timed_repeats"]
