from timeit import default_timer, default_repeat, Timer
import traceback
from functools import wraps
from typing import Callable

_globals = globals


def timed_simple(func : Callable) -> Callable:
    """Simple decorator function to time a function call

    Args:
        func: Any callable to be timed
        
    Returns:
        Wrapped form of func that will `print` timing information when called

    Example:
        >>> @timed_simple
        ... def spam_and_eggs(num):
        ...     print(num)
        ...
        >>> spam_and_eggs(42)
        [TIMING] Running spam_and_eggs ...
        42
        [TIMING] Finished spam_and_eggs in 1.9863990019075572e-05 s
    """

    name = func.__name__

    @wraps(func)
    def wrapped(*args, **kwargs):
        print("[TIMING] Running", name, "...")
        start = default_timer()
        res = func(*args, **kwargs)
        elapsed = default_timer() - start
        print("[TIMING] Finished", name, "in", elapsed, "s")
        return res
    return wrapped


def timeit(  # pylint: disable=too-many-locals, too-many-arguments
        stmt : str,
        setup : str="pass",
        globals : dict = None,  # pylint: disable=redefined-builtin
        number : int = 0,
        repeat : int = default_repeat,
        timer=default_timer,
        verbose : int = 0,
        precision : int =3,
        stat : str = "best",
        ):
    """
    Clone from standard module
    """
    timer = Timer(stmt, setup, timer, globals=globals)
    if number == 0:
        # determine number so that 0.2 <= total time < 2.0
        for i in range(1, 10):
            number = 10**i
            try:
                time_taken = timer.timeit(number)
            except Exception:  # pylint: disable=broad-except
                timer.print_exc()
                return 1
            if verbose:
                print("%d loops -> %.*g secs" % (number, precision, time_taken))
            if time_taken >= 0.2:
                break
    try:
        times = timer.repeat(repeat, number)
    except Exception:  # pylint: disable=broad-except
        timer.print_exc()
        return 1

    if stat == "best":
        best = min(times)
    elif stat == "average":
        best = sum(times) / len(times)
    if verbose:
        print("raw times:", " ".join(["%.*g" % (precision, time_taken) for time_taken in times]))
    print("%d loops," % number, end="")
    usec = best * 1e6 / number
    msec = usec / 1000
    sec = msec / 1000
    if usec < 1000:
        print("%s of %d: %.*g usec per loop" % (
            stat, repeat, precision, usec))
    else:
        if msec < 1000:
            print("%s of %d: %.*g msec per loop" % (
                stat, repeat, precision, msec))
        else:
            print("%s of %d: %.*g sec per loop" % (
                stat, repeat, precision, sec))
    return sec


class TimerContext(object):
    def __init__(self, verbose=True, name=""):
        self.verbose = verbose
        self.name = name
        self.timer = default_timer

    def __enter__(self):
        self.start = self.timer()
        return self

    def __exit__(self, *args):
        end = self.timer()
        self.elapsed_secs = end - self.start
        self.elapsed = self.elapsed_secs * 1000  # millisecs
        if self.verbose:
            print('Finished {} - elapsed time: {:g} ms'.format(
                self.name, self.elapsed))


def timed(func,  # pylint: disable=too-many-arguments
        verbose=False,
        number_in=0,
        precision=3,
        repeat=default_repeat,
        stat="best",
        total_time=2.0):
    """
    Decorator, similar to timeit, but for a function input
    """
    time_per_loop_calibration = total_time / 10
    name = func.__name__

    @wraps(func)
    def wrapped(*args, **kwargs):  # pylint: disable=too-many-branches, too-many-locals
        number = number_in
        if verbose:
            print("Timing:", name)
        if number == 0:
            # determine number so that 0.2 <= total time < 2.0
            for i in range(1, 10):
                number = 10**i
                try:
                    start = default_timer()
                    for _ in range(number):
                        res = func(*args, **kwargs)
                    time_taken = default_timer() - start
                except Exception:  # pylint: disable=broad-except
                    traceback.print_exc()
                    return 1, {}
                if verbose:
                    print("%d loops -> %.*g secs" % (
                        number, precision, time_taken))
                if time_taken >= time_per_loop_calibration:
                    break
        times = []
        try:
            for numr in range(repeat):
                start = default_timer()
                for numn in range(number):
                    res = func(*args, **kwargs)
                times.append(default_timer() - start)
        except Exception:
            traceback.print_exc()
            return 1, {}

        if stat == "best":
            best = min(times)
        elif stat == "average":
            best = sum(times) / len(times)
        usec = best * 1e6 / number
        msec = usec / 1000
        sec = msec / 1000
        timing_info = {
            'statistic': stat,
            'repeats': repeat,
            'loops': number,
            'raw_times': times,
            'seconds-per-loop': sec
        }
        if not verbose:
            return res, timing_info

        print("raw times:", " ".join(["%.*g" % (precision, time_taken) for time_taken in times]))
        print(f"{number:d} loops,", end="")

        if usec < 1000:
            print("%s of %d: %.*g usec per loop" % (
                stat, repeat, precision, usec))
        else:
            if msec < 1000:
                print("%s of %d: %.*g msec per loop" % (
                    stat, repeat, precision, msec))
            else:
                print("%s of %d: %.*g sec per loop" % (
                    stat, repeat, precision, sec))
        return res, timing_info
    return wrapped
